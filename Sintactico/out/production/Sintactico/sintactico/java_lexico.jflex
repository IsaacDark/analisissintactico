package sintactico;
import java_cup.runtime.Symbol;
%%
%class JavaLexico
%public
%unicode
%cup
%full
%type java_cup.runtime.Symbol
%char
%line
L=[a-zA-Z_]
D=[0-9]
ESPACIO=[ \t\r\n]
MULTILINEA="/*"( [^*] | (\*+[^*/]) )*\*+\/
UNILINEA="//"[^\n\r]*(\n|\r|\n\r|\r\n)
CA="@\"".*"\""
%{
    private Symbol symbol(int type, Object value){
        return new Symbol(type, yyline, yycolumn, value);
    }

    private Symbol symbol(int type){
        return new Symbol(type, yyline, yycolumn);
    }
%}
%%
{D}+ {System.out.println("Es un digito "+ yytext()); return new Symbol(sym.D, yyline, yycolumn, yytext());}
{ESPACIO}+ {System.out.println("Es un espacio " + yytext()); /*return new Symbol(sym.ESPACIO, yyline, yycolumn, yytext());*/}
{MULTILINEA} {System.out.println("Es un comentario multilinea " + yytext()); /*return new Symbol(sym.MULTILINEA, yyline, yycolumn, yytext());*/}
{UNILINEA} {System.out.println("Es un unilinea " + yytext()); /*return new Symbol(sym.UNILINEA, yyline, yycolumn, yytext());*/}
"int" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.INT, yyline, yycolumn, yytext());}
"float" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.FLOAT, yyline, yycolumn, yytext());}
"double" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.DOUBLE, yyline, yycolumn, yytext());}
"char" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.CHAR, yyline, yycolumn, yytext());}
"long" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.LONG, yyline, yycolumn, yytext());}
"short" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.SHORT, yyline, yycolumn, yytext());}
"byte" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.BYTE, yyline, yycolumn, yytext());}
"unsigned" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.UNSIGNED, yyline, yycolumn, yytext());}
"boolean" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.BOOLEAN, yyline, yycolumn, yytext());}
"signed" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.SIGNED, yyline, yycolumn, yytext());}
"void" {System.out.println("Es un tipo de dato "+ yytext()); return new Symbol(sym.VOID, yyline, yycolumn, yytext());}

"#import" {System.out.println("Es un import "+ yytext()); return new Symbol(sym.IMPORT, yyline, yycolumn, yytext());}

"const" {System.out.println("Es un modificador de tipo "+ yytext()); return new Symbol(sym.CONST, yyline, yycolumn, yytext());}
"static" {System.out.println("Es un modificador de tipo "+ yytext()); return new Symbol(sym.STATIC, yyline, yycolumn, yytext());}

"@property" {System.out.println("Es un modificador de propiedad "+ yytext()); return new Symbol(sym.PROPERTY, yyline, yycolumn, yytext());}
"@synthesize" {System.out.println("Es un modificador de propiedad "+ yytext()); return new Symbol(sym.SYNTHESIZE, yyline, yycolumn, yytext());}
"@dynamic" {System.out.println("Es un modificador de propiedad "+ yytext()); return new Symbol(sym.DYNAMIC, yyline, yycolumn, yytext());}

"@public" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.PUBLIC, yyline, yycolumn, yytext());}
"@protected" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.PROTECTED, yyline, yycolumn, yytext());}
"@private" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.PRIVATE, yyline, yycolumn, yytext());}

"if" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.IF, yyline, yycolumn, yytext());}
"else" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.ELSE, yyline, yycolumn, yytext());}
"do" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.DO, yyline, yycolumn, yytext());}
"switch" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.SWITCH, yyline, yycolumn, yytext());}
"case" {System.out.println("Es un modificador de acceso "+ yytext()); return new Symbol(sym.CASE, yyline, yycolumn, yytext());}
"while" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.WHILE, yyline, yycolumn, yytext());}
"break" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.BREAK, yyline, yycolumn, yytext());}
"goto" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.GOTO, yyline, yycolumn, yytext());}
"for" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.FOR, yyline, yycolumn, yytext());}
"continue" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.CONTINUE, yyline, yycolumn, yytext());}
"return" {System.out.println("Es una estructura de control "+ yytext()); return new Symbol(sym.RETURN, yyline, yycolumn, yytext());}

"@interface" {System.out.println("Es herencia "+ yytext()); return new Symbol(sym.INTERFACE, yyline, yycolumn, yytext());}
"@implementation" {System.out.println("Es herencia "+ yytext()); return new Symbol(sym.IMPLEMENTATION, yyline, yycolumn, yytext());}

"@try" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.TRY, yyline, yycolumn, yytext());}
"@catch" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.CATCH, yyline, yycolumn, yytext());}
"@throw" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.THROW, yyline, yycolumn, yytext());}
"@finally" {System.out.println("tipo de excepcion "+ yytext()); return new Symbol(sym.FINALLY, yyline, yycolumn, yytext());}

"@class" {System.out.println("Es delimitador de clase "+ yytext()); return new Symbol(sym.CLASS, yyline, yycolumn, yytext());}
"@end" {System.out.println("Es delimitador de clase "+ yytext()); return new Symbol(sym.END, yyline, yycolumn, yytext());}

"@protocol" {System.out.println("Es protocolo "+ yytext()); return new Symbol(sym.PROTOCOL, yyline, yycolumn, yytext());}
"@required" {System.out.println("Es protocolo "+ yytext()); return new Symbol(sym.REQUIRED, yyline, yycolumn, yytext());}
"@optional" {System.out.println("Es protocolo "+ yytext()); return new Symbol(sym.OPTIONAL, yyline, yycolumn, yytext());}

"@selector" {System.out.println("Es selector compilado "+ yytext()); return new Symbol(sym.SELECTOR, yyline, yycolumn, yytext());}
"@autoreleasepool" {System.out.println("Son objetos en memoria "+ yytext()); return new Symbol(sym.AUTOPOOL, yyline, yycolumn, yytext());}
";" {System.out.println("Es delimitador "+ yytext()); return new Symbol(sym.DELIMITADOR, yyline, yycolumn, yytext());}

"+" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.MAS, yyline, yycolumn, yytext());}
"-" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.MENOS, yyline, yycolumn, yytext());}
"/" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.ENTRE, yyline, yycolumn, yytext());}
"*" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.POR, yyline, yycolumn, yytext());}
"%" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.MODULO, yyline, yycolumn, yytext());}
"++" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.INCREMENTO, yyline, yycolumn, yytext());}
"--" {System.out.println("Es un operador aritmetico "+ yytext()); return new Symbol(sym.DECREMENTO, yyline, yycolumn, yytext());}

"," {System.out.println("Es un separador "+ yytext()); return new Symbol(sym.COMA, yyline, yycolumn, yytext());}
":" {System.out.println("Es un separador "+ yytext()); return new Symbol(sym.DOS_PUNTOS, yyline, yycolumn, yytext());}


"&&" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.Y_LOGICO, yyline, yycolumn, yytext());}
"||" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.OR_LOGICO, yyline, yycolumn, yytext());}
"!" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.NO, yyline, yycolumn, yytext());}
"<" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MENOR_QUE, yyline, yycolumn, yytext());}
">=" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MAYOR_O_IGUAL, yyline, yycolumn, yytext());}
"<=" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MENOR_O_IGUAL, yyline, yycolumn, yytext());}
">" {System.out.println("Es un operador relacional "+ yytext()); return new Symbol(sym.MAYOR_QUE, yyline, yycolumn, yytext());}


"&" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.ADN_BIT_A_BIT, yyline, yycolumn, yytext());}
"|" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.BITWISE_INCLUSIVE_OR, yyline, yycolumn, yytext());}
"^" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.EXCLUSIVO_O, yyline, yycolumn, yytext());}
"~" {System.out.println("Es un operador de bit a bit "+ yytext()); return new Symbol(sym.COMPLEMENTO_UNARIO, yyline, yycolumn, yytext());}

"=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.IGUAL, yyline, yycolumn, yytext());}
"+=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.ADICION, yyline, yycolumn, yytext());}
"-=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.RESTA, yyline, yycolumn, yytext());}
"*=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.MULTIPLICACION, yyline, yycolumn, yytext());}
"/=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.DIVICION, yyline, yycolumn, yytext());}
"<<=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.MAYUS_IZQUIERDA, yyline, yycolumn, yytext());}
">>=" {System.out.println("Es un operador de asignacion "+ yytext()); return new Symbol(sym.MAYUS_DERECHA, yyline, yycolumn, yytext());}

"==" {System.out.println("Es un operador de comparacion "+ yytext()); return new Symbol(sym.IGUAL_A, yyline, yycolumn, yytext());}

"?:" {System.out.println("Es un operador condicional "+ yytext()); return new Symbol(sym.CONDICIONAL, yyline, yycolumn, yytext());}

"(" {System.out.println("Es un parentesis de inicio "+ yytext()); return new Symbol(sym.PARENTESIS_INICIO, yyline, yycolumn, yytext());}
")" {System.out.println("Es una parentesis de cierre " + yytext()); return new Symbol(sym.PARENTESIS_CIERRE, yyline, yycolumn, yytext());}
"[" {System.out.println("Es un corchete de inicio " + yytext()); return new Symbol(sym.CORCHETE_INICIO, yyline, yycolumn, yytext());}
"]" {System.out.println("Es un corchete de cierre " + yytext()); return new Symbol(sym.CORCHETE_CIERRE, yyline, yycolumn, yytext());}
"{" {System.out.println("Es una llave de inicio " + yytext()); return new Symbol(sym.LLAVE_INICIO, yyline, yycolumn, yytext());}
"}" {System.out.println("Es una llave de cierre " + yytext()); return new Symbol(sym.LLAVE_CIERRE, yyline, yycolumn, yytext());}
"->" {System.out.println("Es un operador postfix "+ yytext()); return new Symbol(sym.PUNTERO, yyline, yycolumn, yytext());}
"." {System.out.println("Es un operador postfix "+ yytext()); return new Symbol(sym. PUNTO, yyline, yycolumn, yytext());}

{L}({L}|{D})* {System.out.println("Es un identificador "+ yytext()); return new Symbol(sym.IDENTIFICADOR, yyline, yycolumn, yytext());}
{CA} {System.out.println("Es una cadena "+ yytext()); return new Symbol(sym.CA, yyline, yycolumn, yytext());}
. {System.out.println("Simbolo no reconocido "); return new Symbol(sym.ERROR, yyline, yycolumn, yytext());}