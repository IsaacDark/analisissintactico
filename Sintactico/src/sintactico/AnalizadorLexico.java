package sintactico;

import java_cup.runtime.Symbol;

import java.io.*;

public class AnalizadorLexico {
    public static void main(String[] args) throws IOException {
        File file = new File("src\\sintactico\\demo_sintactico.txt");
        Reader lector = new BufferedReader(new FileReader(file));

        parser sintactico = new parser(new JavaLexico(lector));
        try {

            System.out.println(sintactico.parse());
            System.out.println("Analisis Realizado correctamente");

        } catch (Exception e) {
            Symbol sym = sintactico.getS();
            System.out.println("Error de sintaxis. linea: " + (sym.right + 1) +
                    " Columna: " + (sym.left+1) + ", Texto: \"" + sym.value + "\"");
            System.out.println(e.getMessage());
        }
    }
}
